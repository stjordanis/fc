# fc

File compare utility


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## FC.LSM

<table>
<tr><td>title</td><td>fc</td></tr>
<tr><td>version</td><td>3.03a</td></tr>
<tr><td>entered&nbsp;date</td><td>2005-01-10</td></tr>
<tr><td>description</td><td>File compare utility</td></tr>
<tr><td>keywords</td><td>freedos, file, compare, fc</td></tr>
<tr><td>author</td><td>coldfusion1@geocities.com</td></tr>
<tr><td>maintained&nbsp;by</td><td>Maurizio Spagni &lt;flurmy@freemail.it&gt;</td></tr>
<tr><td>platforms</td><td>dos (C) *** uses FreeDOS kitten</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Fc</td></tr>
</table>
